from functools import reduce

# Function to get the longest name from the file
def get_longest_name():
    file_path = r"C:\Users\magshimim\Desktop\sigit-next.py/names.txt"
    with open(file_path) as file:
        names = (line.strip() for line in file)
        longest_name = max(names, key=len, default='')
    return longest_name

# Function to sum the lengths of all names in the file
def sum_names():
    file_path = r"C:\Users\magshimim\Desktop\sigit-next.py/names.txt"
    with open(file_path) as file:
        total_length = sum(len(line.strip()) for line in file)
    return total_length

# Function to get the shortest names from the file
def get_shortest_names():
    file_path = r"C:\Users\magshimim\Desktop\sigit-next.py/names.txt"
    with open(file_path) as file:
        names = list(line.strip() for line in file)
        min_length = min((len(name) for name in names), default=0)
        shortest_names = '\n'.join(name for name in names if len(name) == min_length)
    return shortest_names

# Function to write the lengths of names to a new file
def write_names_len():
    input_path = r"C:\Users\magshimim\Desktop\sigit-next.py/names.txt"
    output_path = r"C:\Users\magshimim\Desktop\sigit-next.py/name_length.txt"
    with open(input_path) as input_file, open(output_path, "w") as output_file:
        lengths = (str(len(line.strip())) for line in input_file)
        output_file.write('\n'.join(lengths))

# Function to get names of a specific length from the file
def get_names_of_length():
    length = int(input("Enter name length: "))
    file_path = r"C:\Users\magshimim\Desktop\sigit-next.py/names.txt"
    with open(file_path) as file:
        names = (line.strip() for line in file)
        names_of_length = '\n'.join(name for name in names if len(name) == length)
    return names_of_length

def main():
    print(get_names_of_length())

if __name__ == '__main__':
    main()
