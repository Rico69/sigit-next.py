# Generator function for seconds
def gen_secs():
    for second in range(60):
        yield second


# Generator function for minutes
def gen_minutes():
    for minute in range(60):
        yield minute


# Generator function for hours
def gen_hours():
    for hour in range(24):
        yield hour


# Generator function for formatted time (hh:mm:ss)
def gen_time():
    for hour in gen_hours():
        for minute in gen_minutes():
            for second in gen_secs():
                yield f"{hour:02d}:{minute:02d}:{second:02d}"


# Generator function for years, starting from a given year (default is 2024)
def gen_years(start=2019):
    while True:
        yield start
        start += 1


# Generator function for months
def gen_months():
    for month in range(1, 13):
        yield month


# Generator function for days in a given month, considering leap years for February
def gen_days(month, leap_year=True):
    if month in [1, 3, 5, 7, 8, 10, 12]:
        max_days = 31
    elif month in [4, 6, 9, 11]:
        max_days = 30
    elif month == 2 and leap_year:
        max_days = 29
    else:
        max_days = 28
    for day in range(1, max_days + 1):
        yield day


# Generator function for full dates and times
def gen_dates():
    for year in gen_years():
        for month in gen_months():
            for day in gen_days(month, leap_year=(year % 4 == 0)):
                for hour in gen_hours():
                    for minute in gen_minutes():
                        for second in gen_secs():
                            yield f'{day:02d}/{month:02d}/{year:04d} {hour:02d}:{minute:02d}:{second:02d}'


# Main function to print dates at intervals of 10^6 iterations
def main():
    counter = 0
    for date in gen_dates():
        if counter % 10 ** 6 == 0:
            print(date)
        counter += 1


if __name__ == '__main__':
    main()
