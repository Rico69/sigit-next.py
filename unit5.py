class IDcreator:
    MIN_ID = 0
    MAX_ID = 999999999

    def __init__(self, id=0):
        self._id = id

    def __iter__(self):
        return self

    def __next__(self):
        while self._id <= IDcreator.MAX_ID:
            self._id += 1
            if check_id_valid(self._id):
                return self._id
        raise StopIteration


def check_id_valid(id_number):
    def transform_digit(digit, index):
        num = int(digit)
        if index % 2 != 0:
            num *= 2
        if num > 9:
            num -= 9
        return num

    digits = str(id_number)
    transformed_sum = sum(transform_digit(d, i) for i, d in enumerate(digits))
    return transformed_sum % 10 == 0


def id_generator(id=0):
    while id <= IDcreator.MAX_ID:
        id += 1
        if check_id_valid(id):
            yield id
    raise StopIteration


def main():
    id = int(input("Enter ID: "))
    choice = input("Generator or Iterator? (gen/it): ")

    it = iter(IDcreator(id))
    gen = id_generator(id)

    if choice == "gen":
        for _ in range(10):
            print(next(gen))

    elif choice == "it":
        for _ in range(10):
            print(next(it))


if __name__ == '__main__':
    main()
